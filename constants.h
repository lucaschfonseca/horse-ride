//
// Created by lucas on 11/12/2019.
//

#ifndef HORSE_RIDE_CONSTANTS_H
#define HORSE_RIDE_CONSTANTS_H

#define NUMBER_OF_SOLUTIONS "Numero de Solucoes"
#define NUMBER_OF_SETTINGS "Numero de Configuracoes"
#define EXECUTION_TIME "Tempo de Execucao"
#define SECONDS "Segundos"
#define INCORRECT_NUMBER_OF_PARAMETERS "Numero de parametros incorretos"

#endif //HORSE_RIDE_CONSTANTS_H
