#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "stack.h"
#include "constants.h"

int horseRide(int nl, int nc, int **board, int *numConf) {
    const int dc[8] = {2, 1, -1, -2, -2, -1, 1, 2};
    const int dl[8] = {1, 2, 2, 1, -1, -2, -2, -1};
    int row;
    int column;
    int l;
    int c;
    int i;
    int res = 0;

    Element e;
    Element aux;

    Stack *stack;

    *numConf = 0;

    if (nl == 1 && nc == 1) {
        *numConf = 1;
        return 1;
    }

    stack = create();

    if (stack != NULL) {
        for (row = 0; row < nl; row++)
            for (column = 0; column < nc; column++) {
                e.row = row;
                e.column = column;
                e.movement = 0;
                e.movesCount = 1;

                board[row][column] = 1;
                push(stack, e);

                while (!isEmpty(stack)) {
                    peek(stack, &e);

                    l = e.row;
                    c = e.column;

                    for (i = e.movement; i <= 7; i++) {
                        l = e.row + dl[i];
                        c = e.column + dc[i];

                        if (l >= 0 && l < nl && c >= 0 && c < nc && board[l][c] == 0)
                            break;
                    }
                    if (i > 7 && e.movement == 0)
                        (*numConf)++;

                    e.movement = i;

                    if (e.movement < 8) {
                        aux.row = l;//e.row + dl[e.movement];
                        aux.column = c;//e.column + dc[e.movement];
                        aux.movesCount = e.movesCount + 1;

                        e.movement++;
                        update(stack, e);

                        //if(aux.row>=0 && aux.row <nl && aux.column>=0 && aux.column<nc){
                        // if(board[aux.row][aux.column]==0){
                        board[aux.row][aux.column] = aux.movesCount;

                        if (aux.movesCount == nl * nc) {
                            res++;
                            (*numConf)++;
                            board[aux.row][aux.column] = 0;
                        } else {
                            aux.movement = 0;
                            push(stack, aux);
                        }
                        //}
                        // }
                    } else {
                        pop(stack, &e);
                        board[e.row][e.column] = 0;
                    }
                }
            }
    }

    destroy(stack);

    return res;
}

int **createMatrix(int rows, int columns) {
    int **m;
    int i;
    int j;

    m = (int **) malloc(sizeof(int *) * rows);

    if (m) {
        for (i = 0; i < rows; i++) {
            m[i] = (int *) malloc(sizeof(int) * columns);
        }

        for (i = 0; i < rows; i++)
            for (j = 0; j < columns; ++j)
                m[i][j] = 0;
    }

    return m;
}

void destroyMatrix(int **m, int nl) {
    int i;

    for (i = 0; i < nl; i++)
        free(m[i]);

    free(m);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err34-c"

int main(int argc, char *argv[]) {
    int rows;
    int columns;
    int **board;
    int configurations;
    int res;

    time_t begin;
    time_t finish;

    if (argc == 3) {
        columns = atoi(argv[1]);
        rows = atoi(argv[2]);

        begin = time(NULL);
        board = createMatrix(rows, columns);

        res = horseRide(rows, columns, board, &configurations);

        destroyMatrix(board, rows);
        finish = time(NULL);

        printf("\n%s = %d", NUMBER_OF_SOLUTIONS, res);
        printf("\n%s = %d", NUMBER_OF_SETTINGS, configurations);
        printf("\n%s = %.2f %s\n", EXECUTION_TIME, difftime(finish, begin), SECONDS);
    } else
        printf("\n%s", INCORRECT_NUMBER_OF_PARAMETERS);

    return 0;
}

#pragma clang diagnostic pop
