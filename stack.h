//
// Created by lucas on 11/5/2019.
//

#ifndef STACK_STACK_H
#define STACK_STACK_H

typedef struct {
    int row;
    int column;
    int movement;
    int movesCount;
} Element;

typedef struct node {
    Element info;
    struct node *next;
} Node;

typedef struct {
    Node *top;
    int size;
} Stack;

Stack *create();

int push(Stack *s, Element e);

int pop(Stack *s, Element *e);

int peek(Stack *s, Element *e);

void destroy(Stack *s);

int getSize(Stack *s);

int isEmpty(Stack *s);

void update(Stack *s, Element e);

#endif //STACK_STACK_H
