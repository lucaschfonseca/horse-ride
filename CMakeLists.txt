cmake_minimum_required(VERSION 3.15)
project(HorseRide C)

set(CMAKE_C_STANDARD 99)

add_executable(HorseRide main.c constants.h stack.c stack.h)