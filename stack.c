//
// Created by lucas on 11/5/2019.
//

#include "stack.h"
#include <stdlib.h>

Stack *create() {
    Stack *s = (Stack *) malloc(sizeof(Stack));

    if (s) {
        s->size = 0;
        s->top = NULL;
    }

    return s;
}

int push(Stack *s, Element e) {
    Node *node;

    if (!s)
        return 0;

    node = (Node *) malloc(sizeof(Node));

    if (!node)
        return 0;

    node->info = e;
    node->next = s->top;

    s->top = node;
    s->size++;

    return 1;
}

int pop(Stack *s, Element *e) {
    Node *node;

    if (!s || !s->top)
        return 0;

    node = s->top;
    s->top = s->top->next;
    s->size--;

    *e = node->info;
    free(node);

    return 1;
}

int peek(Stack *s, Element *e) {
    if (!s || !s->top)
        return 0;

    *e = s->top->info;

    return 1;
}

void destroy(Stack *s) {
    if (!s)return;

    Node *node;

    while (s->top) {
        node = s->top;
        s->top = node->next;
        free(node);
    }

    free(s);
}

int getSize(Stack *s) {
    return s->size;
}

int isEmpty(Stack *s) {
    return !s->size;
}

void update(Stack *s, Element e) {
    Element aux;

    pop(s, &aux);
    push(s, e);
}